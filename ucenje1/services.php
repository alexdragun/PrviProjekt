<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="Affordable and professional web design">
    <meta name="keywords" content="web design, affordable web design, professional web design">
    <meta name="author" content="Alex Dragun">
    <title>Acme Web Design | Services</title>
    <link rel="stylesheet" href="./css/style.css">
</head>

<body>
    <?php require "nav.php" ?>

    <?php require "newsletterEmail.php" ?>

    <section id="main">
        <div class="container">
            <article id="main-col">
                <h1 class="page-title">Services</h1>
                <ul id="services">
                    <li>
                        <h3>Website Design</h3>
                        <p>sad sfa  gasd gdsghfgh  jfgjfgj   sdfsd  hdhdf.</p>
                        <p>Pricing: 1,000$ - 3,000$</p>
                    </li>
                    <li>
                        <h3>Website Maintenance</h3>
                        <p>sad sfa gasd gdsghfgh jfgjfgj sdfsd hdhdf.</p>
                        <p>Pricing: 250$ per month</p>
                    </li>
                    <li>
                        <h3>Website Hosting</h3>
                        <p>sad sfa gasd gdsghfgh jfgjfgj sdfsd hdhdf.</p>
                        <p>Pricing: 25$ per month</p>
                    </li>
                </ul>
            </article>

            <aside id="sidebar">
                <div class="dark">
                    <h3>Get A Quote</h3>
                    <form class="quote">
                        <div>
                            <label>Name</label><br>
                            <input type="text" placeholder="name">
                        </div>
                        <div>
                            <label>Email</label><br>
                            <input type="text" placeholder="Email Adress">
                        </div>
                        <div>
                            <label>Message</label><br>
                            <textarea placeholder="Message"></textarea>
                        </div>
                        <button class="button_1" type="submit">Send</button>
                    </form>
                </div>
            </aside>
        </div>
    </section>

    <?php require "footer.php" ?>
</body>

</html>