<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="Affordable and professional web design">
    <meta name="keywords" content="web design, affordable web design, professional web design" >
    <meta name="author" content="Alex Dragun">
    <title>Acme Web Design | Welcome</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>

<?php require "nav.php" ?>

<section id="showcase">
    <div class="container">
        <h1>Affordable professional Web Design</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lobortis vehicula odio eu scelerisque. Proin blandit consequat
        maximus. Nam ligula velit, consectetur sed tortor in, rhoncus blandit metus.</p>
    </div>
</section>

<?php require "newsletterEmail.php" ?>

<section id="boxes">
    <div class="container">
        <div class="box">
            <img style="width:90px;" src="./img/html5.png">
            <h3>HTML5 Markup</h3>
            <p>Morbi a nisi quis magna feugiat pharetra sit amet id dolor. Nulla facilisi.</p>
        </div>
        <div class="box">
            <img style="width:90px;" src="./img/css3.png">
            <h3>CSS3 Styling</h3>
            <p>Morbi a nisi quis magna feugiat pharetra sit amet id dolor. Nulla facilisi.</p>
        </div>
        <div class="box">
            <img style="width:120px;" src="./img/graphicdesign.png">
            <h3>Graphics Design</h3>
            <p>Morbi a nisi quis magna feugiat pharetra sit amet id dolor. Nulla facilisi.</p>
        </div>
    </div>
</section>

<?php require "footer.php" ?>
</body>

</html>